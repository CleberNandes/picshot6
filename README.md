## Picshot6

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

#### Aula 01 - Starting a Angular Project

Angular expression `{{ }}` is more frequently used when you have to show it inside a html tag like <h1>{{ title }}</h1>

One way data-binding - the data out from component and go to the template
when we want to use a data-binding attribute we envolve de attribute with square brackets `[attribute]`

`angular.json` has the build configurations

install bootstrap by npm install bootstrap
after installed bootstrap we need tell angular to use it
but is not in index.html we put its link
we put it in `angular.json` build.styles a build.scripts

  "styles": [
    "src/styles.css",
    "node_modules/bootstrap/dist/css/bootstrap.min.css"
  ],
  "scripts": [
    "node_modules/bootstrap/dist/js/bootstrap.min.js"
  ]

##### `Input()` receive data from template
inbound propertys

ccreate another modules.ts to keep the app.module(root) small. helping to keep easier maintance

#### Creates a new module
`ng g m name`
it creates a folder 'name' with a `name.module` inside

always create a module to each component group we have to handle
cause if we put all componests inside appModule it tends to get bigger and difficult to maintain

how photo can have a lot of component it is better if we create a photo module that have just components realative just photos in it.

after create a photo.module we jave to import this module in app.module
but to work we have in photo.module exports all components

all in module.decrations is private
all in module.exports is public 

AULA 03
access api through `HttpClient`
after run api create a property in app.module we need to import 
`import { HttpClientModule } from '@angular/common/http';`
and pass `HttpClientModule` inside `imports: [HttpClientModule]`
its by provider that it make dependency injection

####Service
inside photo folder we create a photo.service
`ng g s photos/photo/photo`

```
  import { HttpClient } from '@angular/common/http';
  import { Observable } from 'rxjs';
  import { Injectable } from '@angular/core';

  const API = 'http://localhost:3000'
  @Injectable({
    providedIn: 'root'
  })
  export class PhotoService {
    constructor(private http: HttpClient) { }
  }
```

then we create a method that access the data from database
```
  listFromUser(userName: string): Observable<Object[]>{
    return this.http
      .get<Object[]>(`${API}/${userName}/photos`);
  }
```
Is a good practice access data as a service and called the data bellow this service, in app.component we use a PhotoService get it as injection on constructor and after the service method we use subscribe
```
  constructor(private photoService: PhotoService) {
    this.photoService
      .listFromUser('flavio')
      .subscribe(photos => {
          (this.photos = photos)
        }, error => console.log(error));
  }
```

to make a requisition we use a http method called get passing the url
it returns a observable 
then we weed to subscribe in that observable to get the data,
inside the subscribe we attribute the return with our object
`.subscribe(photos => this.photos = photos)`

lastly we have to type the return on the get and the property with Object[]
`photos: Object[] = [];`
`this.http.get<Object[]>(url).subscribe()`

#### Model
creating a model to deal with the data, and be possible access the data straight to its property
```
  export interface Photo {
    id: number;
    postDate: Date;
    url: string;
    description: string;
    allowComments:boolean;
    likes: number;
    comments: number;
    userId: number;
  }
```

To change a property changed from api in all over aplication without have to be looking for it we can just `rename symbol`
we goes to class/interface definition from the object right click on the property you want to change and choose `Rename Symbol`/`F2`

#### Constructor just to dependency injection
the logic in constructor we transfer to onInit
```
  constructor(private photoService: PhotoService) {}

  ngOnInit(){
    this.photoService
      .listFromUser('flavio')
      .subscribe(
        photos => {
          (this.photos = photos)
        },
        error => console.log(error));
  }
```
and we take off HttpClientModule to appmodule from photoModule
cause the access to get photos is a PhotoModule responsability

#### Take off the responsability to show photo list from app.component
to some component consistent about photo

page scope = component that equals page

create photo-list.component to be responsable to list the photos
`ng g c photos/photo-list`
after it we transfer all code in app.component to photo-list.component
cause all code in appComponent concerns to photo-list.component

##### patern quotes in imports
by default when we import the vscode put double quotes but typescript and javascripts prefer single quotes
to change this we put some preference in vscode options to always use single quotes
`"javascript.preferences.quoteStyle": "single",`
`"typescript.preferences.quoteStyle":"single"`

#### diretivas and CommumModule
BrowserModule has in it the directives ngFor, ngIf etc...
when we create an other module we cannot import the browsermodule to use those directives cause just appModule can import it, to solve this problem and use directives in other modules we can import CommumModule that is brought inside BrowserModule
`import { CommonModule } from '@angular/common';`

#### Routes
create a new component to see the aplication being routed
`ng g c photos/photo-form`

to route our aplication we need to create a routing.ts
inside app folder we create `app.routing.mofule.ts`
the app.routing.module.ts content is
```
  imports...

  const routes = [
    { path: 'user/flavio', component: PhotoListComponent },
    { path: 'p/add', component: PhotoFormComponent }
  ];
  @NgModule({

  })
  export class AppRoutingModule {}
```
routing is similiar as module but have a const routes where keep all our routes
each route has at last `path` and `component` properties.
to pass a type to our const routes we need to handwork its import from @angular/router
`import { Routes } from '@angular/router';`
and type it this our routes
`const routes: Routes = [`
when typeing the routes we are alert if our make mistakes in name of the routes properties

Now we need import RouterModule in all routing.modules.ts files
```
  @NgModule({
    imports: [ RouterModule.forRoot(routes) ]
  })
```
but to use our const routes in RouterModule we have to pass it as argument to RouterModule through its function called forRoot()

So in AppModule we have to import AppRoutingModule

always when import a RoutingModule in AppModule we need to restart our application in ng serve again

Lastly we need to open a gap in app.component.html to show our routed pages through a router tag
<router-outlet></router-outlet> 
but to work we need to import RouterModule that is being used from AppRoutingModule in @NgModule
```
  @NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
```

#### Inexisting Routes - handle with errors
lets show a 404 page if the route is not find
create a erro.module
`ng g m errors`
create a notfound component to handle with 404
`ng g c errors/notfound`

create a new route in routes inside AppRoutingModule
`{ path: '**', component: NotfoundComponent }`
it will show the notfound component if a route was inexistent

we create a html content for notfound component
import ErrorsModule in AppModule

#### paramms in route
to receive params from route we have to use :param
`{ path: 'user/:userName', component: PhotoListComponent },`

and to get this params we inject another class property in constructor inside PhotoListComponent called ActivatedRoute
```
  constructor(
    private photoService: PhotoService,
    private activatedRoute: ActivatedRoute
  ) { }
```

and to get this param we use the property activatedRoute
`const userName = this.activatedRoute.snapshot.params.userName;`

### Aula05 - Organizing photos list - OnChanges

ngOnChanges receive all posibles changes from inbund properties (@Input())
with that we can call oru method to update the porperty
it has an argument called `changes: SimpleChanges`
we can conditionaly update a property based if has changes.photos in photos inbund property.
```
ngOnChanges(changes: SimpleChanges){
  if(changes.property){
    // do something/update the property photos
  }
}
```

### Aula06 - List Search filter
#### pass the input value to property filter
binding an event to get value from input when keyup
add an input with content bellow
```
<input type="search"
        class="rounded"
        placeholder="Descrição"
        autofocus
        (keyup)="filter = $event.target.value">
```

`()` go from template to component
`[]` go from component to template

add a property filter in component
`filter: string = '';` 

to test our keyup event after input we put 
`{{ filter }}`

as we press a key from keyboard our property filter is updating

#### Pipe to use as a filter 
create a pipe filterDescription
`ng g p photos/photos-list/filter-descr`
```
@Pipe({
  name: 'filterDescr'
})
export class FilterDescrPipe implements PipeTransform {
  transform(photos: Photo[], query: string): Photo[] {
    query = query.trim().toLowerCase();
    if(query){
      return photos.filter(
        photo => photo.description.toLowerCase().includes(query)
      );
    } else {
      return photos;
    }
  }
}
```
and to use this pipe above we filter a array of objects
`<app-photos [photos]="photos | filterDescr: filter"></app-photos>`

implement a message if the filter doesn't match any photo
```
<p class="alert alert-info text-center text-muted"
  *ngIf="!photos.length">
  Nenhuma foto foi encontrada
</p>
```

#### Resolve to just create a component with the data
before navegate to the component it already receive the data list ready
create `photo-list.resolver.ts` and inside put
```
@Injectable({ providedIn: 'root' })

export class PhotoListResolver implements Resolve<Observable<Photo[]>>{
  constructor(private service: PhotoService){ }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    const userName = route.params.userName;    
    return this.service.listFromUser(userName);
  }
}
```
in app.routing.ts in the methos that return PhotoListComponent
we add a new property called resolve with the resolver created above
```
const routes: Routes = [
  {
    path: 'user/:userName',
    component: PhotoListComponent,
    resolve: {
      photos: PhotoListResolver
    }
  },
```
with resolver we needn't use subscribe in the method return to get data

in PhotoListComponent we stop to access the PhotoService and just ask to ActivatedRoutesSnapshop if data is ready

when we receive it our component is render
clean ngOnInit and put the line bellow
`this.photos = this.activatedRoute.data.photos;`

#### Improve the search - debounce: Subject<string>
we just apply our pipe if the user stop a specific time to enter characters
add a new property to PhotoListComponent called Subject
`debounce: Subject<string> = new Subject<string>();`
the subject can emit a value and subscribe to listening a value


in PhotoListComponent instead get the entered value we let it to debounce give a next(receiving the value)
`<input (keyup)="debounce.next($event.target.value)">`

and we subscribe in that debounce to receive the string filter
this subscribe from subject is different cause it keep listening 
instead the subscribe from http that just listening one value and complete
to use the time to after this time apply the filter we have to use debounceTime
we have to import handwork
`import { debounceTime } from 'rxjs/operators';`

then we apply a pipe passing a time before the subscribe on ngOnInit photo-list
```
this.debounce
      .pipe(debounceTime(1000))
      .subscribe(filter => this.filter = filter);
```

ctrl + space = see all possible imports from a module

##### to avoid memory problems we must unsubscribe when our component is destroyed
implements OnDestroy method
```
ngOnDestroy(): void {
    this.debounce.unsubscribe();
  }
```

#### Paginate data from server

if we have a lot of objects from database to show in a listing like 1000
we should paginate this data to a better UX
add a nem method in photosListComponent copied from ListFromUser
receiving a new agument as page:number
`listFromUserPaginate(userName: string, page: number): Observable<Photo[]> {`
add a const params from new HttpParams
`const params = new HttpParams().append('page',page.toString());`
and the get method expects to receive a params after url
`return this.http.get<Photo[]>(`${API}/${userName}/photos`, { params });`

make the resolver called the new paginate method passing the page = 1
`return this.service.listFromUserPaginate(userName, page);`

add html button to photoList.html doing an if else using ng-template
passing a click event to the button to call a load function
```
<div class="text-center" *ngIf="hasMore; else elseHasMore">
  <button class="btn btn-outline-primary">Carregar Mais</button>
</div>
<ng-template #elseHasMore>
  <p class="text-center text-muted">
    Todas as fotos foram carregadas
  </p>
</ng-template>
```
inject in constructor the photo.service
`private photoService: PhotoService,`
add a property hasMore property to photoList
`hasMore: boolean = true;`
adding a property to save the current page
`currentPage: number = 1;`
sabe the current unseName
`userName: string = '';`
receive this userName onInit method
`this.userName = this.activatedRoute.snapshot.params.userName;`
create a load method
```
  load(){
    this.photoService
      .listFromUserPaginate(this.userName, ++this.currentPage)
      .subscribe(photos => {
        // we have to concat to update the list and make angular realize it
        this.photos = this.photos.concat(photos);
        if(!photos.length)
          this.hasMore = false
      });
  }
```

### Aula 07 - 
#### Organizing our application in submodules

#### Using Font Awesome
`npm install font-awesome`

put it in angular.json
`"node_modules/font-awesome/css/font-awesome.css"`

when change angular.json we need to restart cli

#### Adding likes and comments icons

inside photo component we put 2 icons fa-heart-o and fa-comment-o


#### passing data from component child to component mother

we need to use @Output
receive a EventEmitter from angular/core
`@Output() onTyping = new EventEmitter<string>();`
and create an event in mother component
`<app-photo-search (onTyping)="filter = $event">`
and passing the debounce from mother to child
in debounde call
`.subscribe(filter => this.onTyping.emit(filter));`

#### Creating Directives
