import { DarkHoverDirective } from './dark-hover.directive';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [ DarkHoverDirective],
  exports: [ DarkHoverDirective]
})
export class DarkHoverModule {

}
