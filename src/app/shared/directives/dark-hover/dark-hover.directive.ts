import { Directive, ElementRef, HostListener, Renderer, Input } from '@angular/core';



@Directive({
  selector: "[app-dark-hover]"
})
export class DarkHoverDirective {

  @Input() brightness = '70%';

  constructor(private el: ElementRef, private render: Renderer) {}

  @HostListener('mouseover')
  darkOn() {
    this.render.setElementStyle(this.el.nativeElement, "filter", `brightness(${this.brightness})`);
    this.render.setElementStyle(this.el.nativeElement, 'transition', '0.5s');
  }

  @HostListener('mouseleave')
  darkOff() {
    this.render.setElementStyle(this.el.nativeElement, "filter", `brightness(100%)`);
    this.render.setElementStyle(this.el.nativeElement, 'transition', '0.5s');
  }
}




