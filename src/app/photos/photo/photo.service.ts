import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Photo } from './photo';

const API = 'http://localhost:3000'
@Injectable({
  // this option allows use the same instance for anyone who wants use photoservice
  providedIn: 'root'
})

export class PhotoService {
  // when we receive an argument inconstructor with private
  // we already put this element as a class property
  constructor(private http: HttpClient) { }

  listFromUser(userName: string): Observable<Photo[]>{
    return this.http
      .get<Photo[]>(`${API}/${userName}/photos`);
  }

  listFromUserPaginate(userName: string, page: number): Observable<Photo[]> {
    const params = new HttpParams().append('page',page.toString());
    return this.http.get<Photo[]>(`${API}/${userName}/photos`, { params });
  }
}
