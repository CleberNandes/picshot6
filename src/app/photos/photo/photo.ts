export class Photo {
  id: number;
  postDate: Date;
  url: string;
  description: string;
  allowComments:boolean;
  likes: number;
  comments: number;
  userId: number;
}
// To change a property changed from api in all over aplication without
// have to be looking for it we can just`rename symbol`
// we goes to class/interface definition from the object right click on
// the property you want to change and choose `Rename Symbol`/`F2`
