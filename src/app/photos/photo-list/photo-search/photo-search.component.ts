import { Component, OnInit, OnDestroy, EventEmitter, Output, Input }
  from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';


@Component({
  selector: "app-photo-search",
  templateUrl: "./photo-search.component.html"
})
export class PhotoSearchComponent implements OnInit, OnDestroy {
  //delay the search to less requisition
  debounce: Subject<string> = new Subject<string>();
  @Output() onTyping = new EventEmitter<string>();
  @Input() searchValue: string = '';

  ngOnInit(): void {
    this.debounce
      .pipe(debounceTime(500))
      .subscribe(filter => this.onTyping.emit(filter));
    // this subscribe from subject is different cause it keep listening
    //instead the subscribe from http that just listening one value and complete
  }

  ngOnDestroy(): void {
    this.debounce.unsubscribe();
  }
}

