import { NgModule } from '@angular/core';
import { FilterDescrPipe } from './filter-descr.pipe';
import { CommonModule } from '@angular/common';

import { TooltipModule } from "ngx-bootstrap/tooltip";
import { PopoverModule } from "ngx-bootstrap/popover";

import { PhotoListComponent } from './photo-list.component';
import { PhotosComponent } from './photos/photos.component';
import { PhotoModule } from '../photo/photo.module';
import { CardModule } from '../../shared/components/card/card.module';
import { PhotoSearchComponent } from './photo-search/photo-search.component';
import { DarkHoverModule } from '../../shared/directives/dark-hover/dark-hover.module';

@NgModule({
  declarations: [
    PhotoListComponent,
    PhotosComponent,
    FilterDescrPipe,
    PhotoSearchComponent
  ],
  imports: [
    CommonModule,
    PhotoModule,
    TooltipModule.forRoot(),
    PopoverModule,
    CardModule,
    DarkHoverModule
  ]
})
export class PhotoListModule {

}
