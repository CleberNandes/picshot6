import { Pipe, PipeTransform } from '@angular/core';
import { Photo } from '../photo/photo';

@Pipe({
  name: 'filterDescr'
})
export class FilterDescrPipe implements PipeTransform {
  transform(photos: Photo[], query: string): Photo[] {
    query = query.trim().toLowerCase();
    if(query){
      return photos.filter(
        photo => photo.description.toLowerCase().includes(query)
      );
    } else {
      return photos;
    }
  }
}
