import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Photo } from '../photo/photo';
import { PhotoService } from '../photo/photo.service';

@Component({
  selector: "app-photo-list",
  templateUrl: "./photo-list.component.html",
  styleUrls: ["./photo-list.component.css"]
})
export class PhotoListComponent implements OnInit {
  title = "PicShot";
  photos: Photo[] = [];
  filter: string = "";


  // load button
  @Input() hasMore: boolean = true;
  currentPage: number = 1;
  userName: string = '';

  constructor(
    private photoService: PhotoService,
    private activatedRoute: ActivatedRoute,

  ) {}

  ngOnInit(): void {
    // const userName = this.activatedRoute.params.userName;
    // this.photoService.listFromUser(userName).subscribe(
    //   photos => {
    //     this.photos = photos;
    //   },
    //   error => console.log(error)
    // );
    this.userName = this.activatedRoute.snapshot.params.userName;
    this.photos = this.activatedRoute.snapshot.data.photos;
  }

  load(){
    this.photoService
      .listFromUserPaginate(this.userName, ++this.currentPage)
      .subscribe(photos => {
        this.filter = '';
        // we have to concat to update the list and make angular realize it
        this.photos = this.photos.concat(photos);
        if(!photos.length)
          this.hasMore = false
      });
  }
}
