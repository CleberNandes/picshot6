import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Photo } from '../../photo/photo';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnChanges {
  @Input() photos: Photo[] = [];
  rows: Photo[][] = [];

  constructor() { }

  // toda vez que photos é mudado chama essa atualização
  ngOnChanges(changes: SimpleChanges) {
    if(changes.photos){
      this.rows = this.groupPhotos(this.photos)
    }
  }
  // separa um array de fotos em diversos arrays com 4 fotos em cada
  groupPhotos(photos: Photo[]): Photo[][] {
    const mountPhotos = [];
    for (let index = 0; index < photos.length; index += 4) {
      mountPhotos.push(photos.slice(index, index + 4));
    }
    return mountPhotos;
  }

}
